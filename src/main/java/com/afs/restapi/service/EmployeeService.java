package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeCreateResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;


import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.toEntity;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeCreateResponse> findAll() {
        return employeeRepository.findAllByStatusTrue().stream().map(EmployeeMapper::getEmployeeCreateResponse)
                .collect(Collectors.toList());
    }

    public Employee update(int id, EmployeeUpdateRequest employeeUpdateRequest) {
        Employee employee = findById(id);
        if (employeeUpdateRequest.getAge() != null) {
            employee.setAge(employeeUpdateRequest.getAge());
        }
        if (employeeUpdateRequest.getSalary() != null) {
            employee.setSalary(employeeUpdateRequest.getSalary());
        }
        employeeRepository.save(employee);
        return employee;
    }

    public Employee findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }
    public EmployeeCreateResponse findEmployeeCreateResponseById(int id) {
        return EmployeeMapper.getEmployeeCreateResponse(employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new));
    }

    public List<EmployeeCreateResponse> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender).stream().map(EmployeeMapper::getEmployeeCreateResponse)
                .collect(Collectors.toList());
    }

    public List<EmployeeCreateResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList().stream()
                .map(EmployeeMapper::getEmployeeCreateResponse).collect(Collectors.toList());
    }

    public Employee insert(EmployeeCreateRequest employeeCreateRequest) {
        return employeeRepository.save(toEntity(employeeCreateRequest));
    }

    public void delete(int id) {
        Employee employee = findById(id);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
