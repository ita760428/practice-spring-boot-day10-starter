package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyCreateResponse;
import com.afs.restapi.dto.EmployeeCreateResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyCreateResponse> getAll() {
        return companyRepository.findAll().stream().map(CompanyMapper::getCompanyCreateResponse)
                .collect(Collectors.toList());
    }

    public List<CompanyCreateResponse> getAll(Integer page, Integer pageSize) {
        return companyRepository.findAll(PageRequest.of(page, pageSize)).toList().stream()
                .map(CompanyMapper::getCompanyCreateResponse).collect(Collectors.toList());
    }

    public CompanyCreateResponse findById(Integer companyId) {
        Company company = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        return CompanyMapper.getCompanyCreateResponse(company);
    }

    public Company create(CompanyCreateRequest companyCreateRequest) {
        return companyRepository.save(CompanyMapper.toEntity(companyCreateRequest));
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public Company update(Integer companyId, Company updatingCompany) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        if (updatingCompany.getName() != null) {
            company.setName(updatingCompany.getName());
        }
        return companyRepository.save(company);
    }

    public List<EmployeeCreateResponse> getEmployees(Integer companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees().stream().map(EmployeeMapper::getEmployeeCreateResponse).collect(Collectors.toList());
    }
}
