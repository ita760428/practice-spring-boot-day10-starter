package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyCreateResponse;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company toEntity(CompanyCreateRequest companyCreateRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyCreateRequest, company);
        return company;
    }

    public static CompanyCreateResponse getCompanyCreateResponse(Company company) {
        CompanyCreateResponse companyCreateResponse = new CompanyCreateResponse();
        BeanUtils.copyProperties(company,companyCreateResponse);
        companyCreateResponse.setEmployeeCount(company.getEmployees().size());
        return companyCreateResponse;
    }
}