package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeCreateResponse;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public  static Employee toEntity (EmployeeCreateRequest employeeCreateRequest){
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeCreateRequest,employee);
        return employee;
    }


    public static EmployeeCreateResponse getEmployeeCreateResponse(Employee employee) {
        EmployeeCreateResponse employeeCreateResponse = new EmployeeCreateResponse();
        BeanUtils.copyProperties(employee,employeeCreateResponse);
        return employeeCreateResponse;
    }
}
