package com.afs.restapi.controller;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyCreateResponse;
import com.afs.restapi.dto.EmployeeCreateResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyCreateResponse> getAll() {
        return companyService.getAll();
    }

    @GetMapping(params = {"page", "size"})
    public List<CompanyCreateResponse> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size
    ) {
        return companyService.getAll(page, size);
    }

    @GetMapping("/{companyId}")
    public CompanyCreateResponse get(@PathVariable Integer companyId) {
        return companyService.findById(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<EmployeeCreateResponse> getEmployees(@PathVariable Integer companyId) {
        return companyService.getEmployees(companyId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company create(@RequestBody CompanyCreateRequest companyCreateRequest) {
        return companyService.create(companyCreateRequest);
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Integer companyId) {
        companyService.deleteCompany(companyId);
    }

    @PutMapping("/{companyId}")
    public Company update(@PathVariable Integer companyId, @RequestBody Company companyRequest) {
        return companyService.update(companyId, companyRequest);
    }
}
