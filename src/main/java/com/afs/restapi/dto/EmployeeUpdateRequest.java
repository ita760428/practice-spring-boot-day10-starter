package com.afs.restapi.dto;

public class EmployeeUpdateRequest {
    private Integer id;
    private Integer age;

    private Integer salary;

    private Integer companyId;

    public EmployeeUpdateRequest(Integer id, Integer age, Integer salary,Integer companyId) {
        this.id = id;
        this.age = age;
        this.salary = salary;
        this.companyId = companyId;
    }

    public EmployeeUpdateRequest() {
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
