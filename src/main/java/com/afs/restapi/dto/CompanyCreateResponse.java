package com.afs.restapi.dto;

public class CompanyCreateResponse {
    private Integer id;
    private String name;
    private Integer employeeCount;

    public CompanyCreateResponse() {
    }

    public CompanyCreateResponse(Integer id, String name, Integer employeeCount) {
        this.id = id;
        this.name = name;
        this.employeeCount = employeeCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(Integer employeeCount) {
        this.employeeCount = employeeCount;
    }
}
